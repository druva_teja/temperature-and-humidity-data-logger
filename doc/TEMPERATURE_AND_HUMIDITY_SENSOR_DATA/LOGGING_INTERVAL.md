**AUTHOR :- ANURAG SINGH**

**MR6662** is a card size data logger, which measures temperature and humidity and is capable of storing 6,000 data for each. Temperature sensors are capacitance type, so they are compatible. Data recalling and setting are also available on PC. • Number of logging data is 6,000 data for temperature and humidity each. Measurement interval can be selected between 1 to 60min.

±0.5°C (between -5 to 50°C) of high accuracy
Logging data can be stored even in dead battery.


[MR6662 complete logging interval detail](https://www.chino.co.jp/english/products/loggers/mr6662/#:~:text=MR6662%20is%20a%20card%20size,storing%206%2C000%20data%20for%20each.&text=Measurement%20interval%20can%20be%20selected,stored%20even%20in%20dead%20battery.)`